package com.danit;

import com.danit.binary.BinarySerializer;
import com.danit.json.JSONSerializer;
import com.danit.shape.Circle;
import com.danit.shape.Group;
import com.danit.shape.Shape;
import com.danit.shape.Square;
import com.danit.serializer.Serializer;
import com.danit.xml.XMLSerializer;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SerializerTest {

    @Test
    public void testSquareXML() {
        Shape square = new Square(0, 2, 3);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Serializer serializer = XMLSerializer.getXmlSerializer();
    }

    @Test
    public void verifyThatCircleCanBeSerializedToXML() {
        // given
        Circle circle = new Circle(100, 100, 200);
        Serializer serializer = XMLSerializer.getXmlSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(circle, out);

        // then
        assertThat(out.toString(), is("<circle><x>100</x><y>100</y><radius>200</radius></circle>"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToXML() {
        // given
        Square square = new Square(150, 260, 70);
        Serializer serializer = XMLSerializer.getXmlSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toString(), is("<square><x>150</x><y>260</y><side>70</side></square>"));
    }

    @Test
    public void verifyThatGroupCanBeSerializedToXML() {
        // given
        Group group = new Group();
        Circle circle = new Circle(100, 100, 200);
        Square square = new Square(150, 260, 70);
        group.add(circle);
        group.add(square);
        Serializer serializer = XMLSerializer.getXmlSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(group, out);

        // then
        assertThat(out.toString(), is("" +
                "<group>" +
                "<circle><x>100</x><y>100</y><radius>200</radius></circle>" +
                "<square><x>150</x><y>260</y><side>70</side></square>" +
                "</group>"));
    }

    @Test
    public void verifyThatCircleCanBeSerializedToJSON() {
        // given
        Circle circle = new Circle(100, 100, 200);
        Serializer serializer = JSONSerializer.getJSONSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(circle, out);

        // then
        assertThat(out.toString(), is("{\"x\":100,\"y\":100,\"radius\":200,\"type\":\"circle\"}"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToJSON() {
        // given
        Square square = new Square(150, 260, 70);
        Serializer serializer = JSONSerializer.getJSONSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toString(), is("{\"x\":150,\"y\":260,\"side\":70,\"type\":\"square\"}"));
    }

    @Test
    public void verifyThatGroupCanBeSerializedToJSON() {
        // given
        Group group = new Group();
        Circle circle = new Circle(100, 100, 200);
        Square square = new Square(150, 260, 70);
        group.add(circle);
        group.add(square);
        Serializer serializer = JSONSerializer.getJSONSerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(group, out);

        // then
        assertThat(out.toString(), is("" +
                "[{\"x\":100,\"y\":100,\"radius\":200,\"type\":\"circle\"}," +
                "{\"x\":150,\"y\":260,\"side\":70,\"type\":\"square\"}]"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToBinary() {
        // given
        Square square = new Square(50, 20, 70);
        Serializer serializer = BinarySerializer.getBinarySerializer();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{3, 0, 0, 0, 50, 0, 0, 0, 20, 0, 0, 0, 70}));
    }
}