package com.danit.binary;

import com.danit.serializer.Serializer;
import com.danit.shape.Shape;
import com.danit.shape.Square;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class BinarySerializer implements Serializer<Shape> {
    private static final BinarySerializer binarySerializer = new BinarySerializer();
    private Map<String, Serializer> serializers = new HashMap<>();

    private BinarySerializer() {
        this.serializers.put(Square.class.getCanonicalName(), new SquareBinarySerializer());
        //this.serializers.put(Circle.class.getCanonicalName(), new CircleBinarySerializer());
        //this.serializers.put(Group.class.getCanonicalName(), new GroupBinarySerializer());
    }

    public static Serializer getBinarySerializer() {
        return binarySerializer;
    }

    public Serializer getSerializer(String key) {
        return serializers.get(key);
    }

    @Override
    public void serialize(Shape shape, OutputStream os) {
        Serializer serializer = serializers.get(shape.getType());
        if (serializer != null) {
            serializer.serialize(shape, os);
        }
    }
}
