package com.danit.binary;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Square;

import java.io.OutputStream;

public class SquareBinarySerializer extends AbstractSerializer<Square> implements Serializer<Square> {
    @Override
    public void serialize(Square shape, OutputStream os) {
        byte[] byteStream = new byte[]{3};

        write(os, byteStream);
    }
}
