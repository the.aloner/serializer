package com.danit.json;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Circle;

import java.io.OutputStream;

public class CircleJSONSerializer extends AbstractSerializer<Circle> implements Serializer<Circle> {
    @Override
    public void serialize(Circle shape, OutputStream os) {
        write(os, "{\"x\":");
        write(os, shape.getX());
        write(os, ",\"y\":");
        write(os, shape.getY());
        write(os, ",\"radius\":");
        write(os, shape.getRadius());
        write(os, ",\"type\":\"circle\"}");
    }
}
