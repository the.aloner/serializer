package com.danit.json;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Group;
import com.danit.shape.Shape;

import java.io.OutputStream;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class GroupJSONSerializer extends AbstractSerializer<Group> implements Serializer<Group> {
    @Override
    public void serialize(Group shapeGroup, OutputStream os) {
        StringBuilder res = new StringBuilder();
        List<Shape> shapes = shapeGroup.getShapes();
        int shapesNumber = shapes.size();

        write(os, "[");

        for (int i = 0; i < shapes.size() - 1; i++) {
            JSONSerializer.getJSONSerializer().serialize(shapes.get(i), os);
            write(os, ",");
        }

        if (shapes.size() > 0) {
            JSONSerializer.getJSONSerializer().serialize(shapes.get(shapesNumber - 1), os);
        }

        write(os, "]");

        //shapeGroup.getShapes().forEach(shape -> JSONSerializer.getJSONSerializer().serialize(shape, os));
    }
}
