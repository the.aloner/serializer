package com.danit.json;

import com.danit.shape.Circle;
import com.danit.shape.Group;
import com.danit.shape.Shape;
import com.danit.shape.Square;
import com.danit.serializer.Serializer;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class JSONSerializer implements Serializer<Shape> {
    private static final JSONSerializer jsonSerializer = new JSONSerializer();
    private Map<String, Serializer> serializers = new HashMap<>();

    private JSONSerializer() {
        this.serializers.put(Square.class.getCanonicalName(), new SquareJSONSerializer());
        this.serializers.put(Circle.class.getCanonicalName(), new CircleJSONSerializer());
        this.serializers.put(Group.class.getCanonicalName(), new GroupJSONSerializer());
    }

    public static Serializer getJSONSerializer() {
        return jsonSerializer;
    }

    public Serializer getSerializer(String key) {
        return serializers.get(key);
    }

    @Override
    public void serialize(Shape shape, OutputStream os) {
        Serializer serializer = serializers.get(shape.getType());
        if (serializer != null) {
            serializer.serialize(shape, os);
        }
    }
}
