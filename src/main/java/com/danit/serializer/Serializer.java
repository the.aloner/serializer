package com.danit.serializer;

import java.io.OutputStream;

public interface Serializer<T> {
    void serialize(T shape, OutputStream os);
}
