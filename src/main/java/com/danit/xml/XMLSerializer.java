package com.danit.xml;

import com.danit.serializer.Serializer;
import com.danit.shape.Circle;
import com.danit.shape.Group;
import com.danit.shape.Shape;
import com.danit.shape.Square;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class XMLSerializer implements Serializer<Shape> {
    private final static XMLSerializer xmlSerializer = new XMLSerializer();
    private Map<String, Serializer> serializers = new HashMap<>();

    private XMLSerializer() {
        this.serializers.put(Square.class.getCanonicalName(), new SquareXMLSerializer());
        this.serializers.put(Circle.class.getCanonicalName(), new CircleXMLSerializer());
        this.serializers.put(Group.class.getCanonicalName(), new GroupXMLSerializer());
    }

    public static Serializer getXmlSerializer() {
        return xmlSerializer;
    }

    public Serializer getSerializer(String key) {
        return serializers.get(key);
    }

    @Override
    public void serialize(Shape shape, OutputStream os) {
        Serializer serializer = serializers.get(shape.getType());
        if (serializer != null) {
            serializer.serialize(shape, os);
        }
    }
}
